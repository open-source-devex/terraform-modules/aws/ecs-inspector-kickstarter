# ECS Inspector Kickstarter

Terraform module to create an ECS Inspector kickstarter task.

This task:

- fetches AMIs in the region it's deployed in,
- starts new VMs with this image and the inspector agent
- kicks off an inspector scan
- published some finding metadata about high findings to an SNS topic

## Requirements

- ECS cluster
- VPC & subnet with outbound connectivity (needed for installing inspector agent)

## Outputs

An SNS topic for basic alerting.
