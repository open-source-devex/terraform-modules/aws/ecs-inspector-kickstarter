terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-ecs-inspector-kickstarter-simple"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

module "inspector-kickstarter" {
  enable = true

  source = "../../"

  aws_region = "eu-west-1"
  name       = "test-inspector-kickstarter"

  assessment_instance_subnet_ids = module.vpc.private_subnets

  vpc_id         = module.vpc.vpc_id
  ecs_cluster_id = module.ecs.this_ecs_cluster_id

  cloudwatch_cron_schedule = "cron(0 3 * * ? *)"
}



module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "test-inspector-kickstarter"

  cidr = "10.0.0.0/16"

  azs             = ["eu-west-1a"]
  public_subnets  = ["10.0.100.0/24"]
  private_subnets = ["10.0.101.0/24"]

  enable_nat_gateway = true
}

module "ecs" {
  source  = "terraform-aws-modules/ecs/aws"
  version = "v2.0.0"

  name = "test-inspector-kickstarter"
}
